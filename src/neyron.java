import java.util.ArrayList;

/**
 * Created by repeat on 8/26/15.
 */
public class neyron {
    public int layer;
    public int idx_neyron;
    public double Fe;
    public double w0 = -1.5+Math.random()*3;
    public double local_gradient = 0;
    private ArrayList<Double> weight = new ArrayList<Double>();
    public ArrayList<Double> delta_weight = new ArrayList<Double>();

    neyron (int layer, int idx_neyron) {
        this.layer = layer;
        this.idx_neyron = idx_neyron;
        //this.Fe = this.calcFe();

//        System.out.println("w0="+w0);
    }

    public double getWeight(int from) {
//TODO check for out of range
        return weight.get(from);
    }

    public void setWeight(int from, double w) {
        // ArrayList can't add any index only continuously
//TODO check this
        if (from >= weight.size())
            weight.add(from, w);
        else
            weight.set(from, w);
    }

    public void setWeight(double w) {
        w0 = w;
    }

    public int getLayer() {
        return layer;
    }

    public int getIdxNeyron() {
        return idx_neyron;
    }
}
