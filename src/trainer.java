import build.tools.javazic.*;

import java.util.ArrayList;

/**
 * Created by repeat on 8/29/15.
 */

// k(in) - i - j(out)

    /*
    Если ошибка error_out сети превышает допустимое значение, требуется продолжить обучение сети.
     */

public class trainer {

    private double[] target;
    private double n; // learning-rate parameter
    private NET NET;

    trainer() {
    }

    public void training(NET NeuronNetwork, double[] input, double[] target, double n) {
        this.target = target;
        this.n = n;
        this.NET = NeuronNetwork;
        //TODO check number neyrons in layer and input data
        for (int i = 0; i < NET.getNumNeyrons(0); ++i) {
            NET.getNeyron(0, i).Fe = input[i];
        }
        // считаем выход нейронов
        NET.getOut();
        // подсчитываем ошибку и корректируем веса
        for (int l = NET.Layers.size() - 1; l > 0; --l) {
            for (int i = 0; i < NET.getNumNeyrons(l); ++i) {
                d(l, i);
            }
        }
    }

    private double v(int layer, int j_neyron) {
        return NET.getNeyron(layer,j_neyron).Fe;
    }

    private double e(int j_neyron) {
        return this.target[j_neyron] - NET.getNeyron(NET.Layers.size()-1, j_neyron).Fe;
    }

    private void d(int layer, int j_neyron) {
        if (layer == NET.Layers.size()-1) {
            NET.getNeyron(layer,j_neyron).local_gradient = e(j_neyron) * v(layer,j_neyron)*(1-v(layer,j_neyron));
            recalcWeights(layer, j_neyron);
        } else {
            double sum = 0;
            for (int nextLayer_neyron=0; nextLayer_neyron<NET.getNumNeyrons(layer+1); ++nextLayer_neyron) {
                sum += NET.getNeyron(layer+1,nextLayer_neyron).local_gradient * NET.getNeyron(layer+1,nextLayer_neyron).getWeight(j_neyron);
            }
            NET.getNeyron(layer,j_neyron).local_gradient = sum * v(layer,j_neyron)*(1-v(layer,j_neyron));
            recalcWeights(layer, j_neyron);
        }
    }

    private void recalcWeights(int layer, int j_neyron) {
        double local_gradient = NET.getNeyron(layer,j_neyron).local_gradient;
        double yi;
        double wij;
        for (int pn = 0; pn<NET.getNumNeyrons(layer-1); ++pn) {
            wij = NET.getNeyron(layer,j_neyron).getWeight(pn);
            yi = NET.getNeyron(layer-1, pn).Fe;
            //NET.getNeyron(layer,j_neyron).delta_weight.add(prevLayer_neyron, trainer.n * local_gradient * yi);
            NET.getNeyron(layer,j_neyron).setWeight(pn, wij + this.n * local_gradient * yi) ;
        }
        NET.getNeyron(layer, j_neyron).w0 = NET.getNeyron(layer, j_neyron).w0 + this.n * NET.getNeyron(layer,j_neyron).local_gradient * (+1);
    }
}