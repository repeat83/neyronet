import javax.swing.*;
import java.awt.*;
import java.awt.geom.Arc2D;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

class Constants {
    public static final boolean DEBUG = true;
}

public class Main {

    public static final boolean DEBUG = true;

    public static NET NET = new NET();

    public static void main(String[] args) throws InterruptedException {
        Canvas c = new Canvas();
        JFrame frame = new JFrame();
        //frame.setBounds(100, 100, 192, 108);
        frame.getContentPane().setPreferredSize(new Dimension(500, 500));
        frame.setTitle("Points");
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(c); // Add canvas data to jframe
        frame.setVisible(true);

        NET.createLayer(3);
        NET.createLayer(2);
        //NET.createLayer(5);
        NET.createLayer(3);

        ArrayList<double[]> I = new ArrayList<>();
        ArrayList<double[]> Ans = new ArrayList<>();

/*        I.add(new double[] {0,100,Math.PI});
        Ans.add(new double[] {0,1});
        I.add(new double[] {0,200,Math.PI});
        Ans.add(new double[] {0,1});
        I.add(new double[] {0,300,Math.PI});
        Ans.add(new double[] {0,1});
        I.add(new double[] {0,400,Math.PI});
        Ans.add(new double[] {0,1});
        I.add(new double[] {-10,100,Math.PI});
        Ans.add(new double[] {0,1});
        I.add(new double[] {-10,200,Math.PI});
        Ans.add(new double[] {0,1});
        I.add(new double[] {-10,300,Math.PI});
        Ans.add(new double[] {0,1});
        I.add(new double[] {-10,400,Math.PI});
        Ans.add(new double[] {0,1});

        I.add(new double[] {800,100,0});
        Ans.add(new double[] {Math.PI,1});
        I.add(new double[] {800,200,0});
        Ans.add(new double[] {Math.PI,1});
        I.add(new double[] {800,300,0});
        Ans.add(new double[] {Math.PI,1});
        I.add(new double[] {800,400,0});
        Ans.add(new double[] {Math.PI,1});
        I.add(new double[] {810,100,0});
        Ans.add(new double[] {Math.PI,1});
        I.add(new double[] {810,200,0});
        Ans.add(new double[] {Math.PI,1});
        I.add(new double[] {810,300,0});
        Ans.add(new double[] {Math.PI,1});
        I.add(new double[] {810,400,0});
        Ans.add(new double[] {Math.PI,1});

        I.add(new double[] {100,0,Math.PI/2});
        Ans.add(new double[] {(3/2*Math.PI) / (2*Math.PI),1});
        I.add(new double[] {200,0,Math.PI/2});
        Ans.add(new double[] {(3/2*Math.PI) / (2*Math.PI),1});
        I.add(new double[] {300,0,Math.PI/2});
        Ans.add(new double[] {(3/2*Math.PI) / (2*Math.PI),1});
        I.add(new double[] {400,0,Math.PI/2});
        Ans.add(new double[] {(3/2*Math.PI) / (2*Math.PI),1});
*/

        I.add(new double[] {0,0,0});
        Ans.add(new double[] {0,0,0});

        I.add(new double[] {0,0,1});
        Ans.add(new double[] {0,0,1});

        I.add(new double[] {0,1,0});
        Ans.add(new double[] {0,1,0});

        I.add(new double[] {0,1,1});
        Ans.add(new double[] {1,1,1});

        I.add(new double[] {1,0,0});
        Ans.add(new double[] {0,0,0});

        I.add(new double[] {1,0,1});
        Ans.add(new double[] {0,0,1});

        I.add(new double[] {1,1,0});
        Ans.add(new double[] {0,1,0});

//        I.add(new double[] {1,1,1});
//        Ans.add(new double[] {0,1,0.5});

        trainer trainer = new trainer();

        neyron last_neyron = NET.getNeyron(NET.Layers.size()-1, 0);
        Color red = new Color(255,0,0);
        Color green = new Color(0,255,0);
        Color blue = new Color(0,0,255);
        Color yellow = new Color(255,255,0);
        Color pink = new Color(255,0,255);
        Color cyan = new Color(0,255,255);

        double start = 100;
        int x,y;
        double iteration=start;
        double step = 1;

        // обучение online - каждый тренировочный "пресет" прогоняется 2-3 раза и выполняется переход к следующему "пресету". НЕ ОПТИМАЛЬНО!!!
        do {
            for (int tr_data = 0; tr_data < I.size(); ++tr_data) {
                x = (int) (frame.getContentPane().getPreferredSize().getWidth() - frame.getContentPane().getPreferredSize().getWidth() * iteration/ start);

                trainer.training(NET, I.get(tr_data), Ans.get(tr_data), step);
                trainer.training(NET, I.get(tr_data), Ans.get(tr_data), step);
                trainer.training(NET, I.get(tr_data), Ans.get(tr_data), step);
                y = 0;
                for (int i = 0; i < NET.getNumNeyrons(NET.Layers.size() - 1); ++i) {
                    y += (int) (NET.getNeyron(NET.Layers.size() - 1, i).local_gradient * 200);
                }
                c.addPoint(new Point(x, y + 200), blue);
                c.draw();
            }
        --iteration;
        } while (iteration>0);

		
		
		
        I.add(new double[] {1,1,1});
		I.add(new double[] {0.5,0.5,0.5});
		I.add(new double[] {0.5,1,1});
		I.add(new double[] {0.2,1,1});
		
		
        for (int idx=0;idx<I.size(); ++idx) {
            NET.setInput(I.get(idx)); // подали на вход
            NET.getOut(); // получить результат
            System.out.println("" + idx);
            System.out.println("Input:   " + I.get(idx)[0] + "\t\t\t" + I.get(idx)[1]  + "\t\t\t" + I.get(idx)[2]);
          if (Ans.size() - 1 >= idx) { System.out.println("Trainer: " + Ans.get(idx)[0] + "\t\t\t" + Ans.get(idx)[1]  + "\t\t\t" + Ans.get(idx)[2]); }
            System.out.print("NetOut:  ");
			for (int i = 0; i < NET.getNumNeyrons(NET.Layers.size() - 1); ++i) {
//                System.out.print(I.get(0) + " ");
                System.out.print(NET.getNeyron(NET.Layers.size() - 1, i).Fe + "\t");
            }
            System.out.println();
            System.out.println();
        }

//debug показать веса
/*		
		for (int l=1;l<NET.Layers.size(); ++l){
            for(int n=0; n<NET.getNumNeyrons(l); ++n) {
                System.out.println("l" + l + " w_" + n + "="+NET.getNeyron(l,n).w0);
                for (int prev_n=0; prev_n<NET.getNumNeyrons(l-1); ++prev_n) {
                    System.out.println("l" + l + " w"+prev_n+n+"="+NET.getNeyron(l,n).getWeight(prev_n));
                }
            }
        }
*/
    }
}
