import java.util.*;

/**
 * Created by repeat on 8/25/15.
 */
public class NET {

    public ArrayList<ArrayList> Layers = new ArrayList<ArrayList>();


    NET() {
        if (Constants.DEBUG) {
            System.out.println("started constrictor");
        }
    }

    //активационная функция
    private double sigmoid (double z) {
        //return z > 0 ? 1 : 0;
        double c = 1;
        return 1/(1 + Math.exp(-z * c));
    }

    public void createLayer(int neyrons) {
        Layers.add(new ArrayList<neyron>());
        int layer = Layers.size() - 1;
        for (int idx=0; idx<neyrons; ++idx) {
            addNeyron(layer);
        }
    }

    public neyron getNeyron(int layer, int neyron){
        if (layer > Layers.size()-1) {
            System.out.println("getNeyron() out of range layer. Using last Layer");
            return (neyron) Layers.get(Layers.size() - 1).get(neyron);
        } else if (neyron >= Layers.get(layer).size()) {
            System.out.println("getNeyron() out of range neyron. Using last neyron in layer");
            return (neyron) Layers.get(layer).get(Layers.get(layer).size()-1);

        }
        return (neyron) Layers.get(layer).get(neyron);
    }

    public int getNumNeyrons(int layer) {
        if (Layers.size() > layer) {
            return Layers.get(layer).size();
        } else {
            return 0;
        }
    }

    public void addNeyron(int layer) {
        int idx = Layers.get(layer).size();
        Layers.get(layer).add(idx, new neyron(layer, idx));

        //init weight for neyrons previous layer
        if (layer > 0) {
            int prev_layer = layer - 1;
            int countNeyronsPrev = this.getNumNeyrons(prev_layer);
//            System.out.println("countNeyronsPrev " + countNeyronsPrev);
//            System.out.println("Prev_layer\tcount\tprev_neyr");
            double weight;
            weight = (idx%2 == 0) ? 0.1 : -0.1;
            for (int prev_neyron = 0; prev_neyron < countNeyronsPrev; ++prev_neyron) {
                weight = -1.5+Math.random()*3;
//              weight = 10*Math.random();
// вес нужно брать случайный, но определённым образом, чтобы с соседним не "конфликтовал"
                this.getNeyron(layer, idx).setWeight(prev_neyron, weight);
//                System.out.println(layer + "w" + prev_neyron + idx + "=" + NET.getNeyron(layer, idx).getWeight(prev_neyron));
                //System.out.println(prev_layer + "\t\t\t" + countNeyronsPrev + "\t\t" + prev_neyron);
            }
        }
        //init weight for neyrons next layer
        if (layer < Layers.size()-1) {
            int next_layer = layer + 1;
            int countNeyronsNext = this.getNumNeyrons(next_layer);
//            System.out.println("countNeyronsNext " + countNeyronsNext);
//            System.out.println("Next_layer\tcount\tnext_neyr");

            for (int next_neyron = 0; next_neyron < countNeyronsNext; ++next_neyron) {
                this.getNeyron(next_layer, next_neyron).setWeight(idx, -1.5+Math.random()*3);
//                System.out.println(next_layer + "\t\t\t" + countNeyronsNext + "\t\t" + next_neyron);
            }
        }
    }

    public void getOut() {
        // start calculate from second layer with index 1
        for (int l = 1; l<this.Layers.size(); ++l) {
//System.out.println("\nprev_layer " + (l-1) + " have " + NET.Layers.get(l-1).size() + " neyrons");
            for (int n = 0; n<this.getNumNeyrons(l); ++n) {
                //NET.getNeyron(l,n).calcFe();
                this.calcFe(l,n);
            }
        }
//TODO выводить массив, если на выходе более 1 нейрона
//        return NET.getNeyron(NET.Layers.size()-1,0).Fe;
    }

    public  void setInput(double[] input) {
        for (int n=0; n<this.getNumNeyrons(0); ++n) {
            this.getNeyron(0,n).Fe = input[n];
        }
    }

    public double calcFe(int layer, int neyron) {
        neyron selected_neyron = this.getNeyron(layer, neyron);
        int prev_layer = layer-1;
        double sum = 0;
        double x;
        double w;
//System.out.print(w0 + " + ");
        int countNeyronsPrev = this.getNumNeyrons(prev_layer);
        for (int prev_neyron = 0; prev_neyron<countNeyronsPrev; ++prev_neyron) {
            x = this.getNeyron(prev_layer,prev_neyron).Fe;
            //w = getWeight(prev_neyron);
            w = selected_neyron.getWeight(prev_neyron);
            sum += w * x;
//System.out.print(w + " * " + x + "\t");
        }
        //Fe = sigmoid(sum + w0);
        selected_neyron.Fe = sigmoid(sum + selected_neyron.w0);
//System.out.println(layer+"Fe"+idx_neyron+"=" + Fe);
        return sigmoid(sum + selected_neyron.w0);
    }

}
