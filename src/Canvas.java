import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Canvas extends JPanel {
    BorderLayout borderLayout1 = new BorderLayout();
    private ArrayList<Point> points;
    private ArrayList<Color> colors;

    public Canvas() {
        points = new ArrayList<Point>();
        colors = new ArrayList<Color>();
        try {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addPoint(Point point, Color color) {
        points.add(point);
        colors.add(color);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for(int i = 0; i < points.size(); i++) {
                g.setColor(colors.get(i));
            g.fillRect(points.get(i).x, points.get(i).y, 2, 2);
        }
    }

    public void draw() {
        repaint();
    }

    void jbInit() throws Exception {
        this.setLayout(borderLayout1);
    }
}
